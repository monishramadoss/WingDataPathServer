var express = require('express');
var path = require('path');
var logger = require('./logger');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose');

var spawn = require('child_process').spawn;

var config = require('./config');

var routes = require('./routes/index');
var tmp = require('./routes/temp');
var ML_Service = require('./routes/ML_Service');
var locationService = require('./routes/locationService');
var customer = require('./routes/customer_1_0');
var fulfiller = require('./routes/fulfiller_1_0');
var login = require('./routes/login');
var item = require('./routes/item');

var app = express();

mongoose.connnect('mongodb://localhost/zadmin/zpippy');
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(require('morgan')({ "stream": logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', routes);
app.use('/temp', tmp);
app.use('/ML_Service', ML_Service);
app.use('/locationService', locationService);
app.use('/customerAPI', customer);
app.use('/fulfillerAPI', fulfiller);
app.use('/login', login);
app.use('/item', item);

var Task_Scheduler_script = __dirname + './python_scripts/Task_Scheduler/main.py';

//process.env.NODE_ENV = 'development';

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


var zookeeper = spawn(config.kafka_server.kafka_zookeeper, [config.kafka_server.kafka_zookeeper_config]);
var kafkaPipe = spawn(config.kafka_server.kafka_start, [config.kafka_server.kafka_start_config]);
var Task_Scheduler = spawn('python', [Task_Scheduler_script])

zookeeper.stderr.on('data', function (data) {
    console.log('stderr: ' + data);
});

kafkaPipe.stderr.on('data', function (data) {
    console.log('stderr: ' + data);
});

Task_Scheduler.stderr.on('data', function (data) {
    console.log('stderr: ' + data);
});

/*
//startup MongodbClient and then startup server
MongoClient.connect(config.mongodb[0], { useNewUrlParser: true, poolSize: 10 }).then(client => {
    dbClient = client;
    app.locals.db = client.db('wingdb');
    app.set('port', process.env.PORT || 1337);
    var server = app.listen(app.get('port'), function () { });
}).catch(error => console.error(error));
*/


process.on('SIGINT', () => {
    dbClient.close();
    process.exit();
    zookeeper.kill();
    kafkaPipe.kill();
    Task_Scheduler.kill();
});