from textblob import TextBlob as tb
import tree_ops as to
import tree_ops as to


# calculate similarity between strings
def levenshtein(s, t):
        if s == t: return 0
        elif len(s) == 0: return len(t)
        elif len(t) == 0: return len(s)
        v0 = [None] * (len(t) + 1)
        v1 = [None] * (len(t) + 1)
        for i in range(len(v0)):
            v0[i] = i
        for i in range(len(s)):
            v1[0] = i + 1
            for j in range(len(t)):
                cost = 0 if s[i] == t[j] else 1
                v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
            for j in range(len(v0)):
                v0[j] = v1[j]
        return v1[len(t)]


def find_relational(sentence, nouns):
    relationships = ["girlfriend","mom","mommy","daddy","mama","papa","dad","mother","father","grandmother","grandfather","friend","aunt","uncle","cousin","brother","sister","older brother","older sister","younger brother","younger sister","little brother","little sister","dog"
    ,"hamster","guinea pig","dog","cat","pet hamster","pig","husband","wife","grandma","grandpa","granddad","son","daughter","child"]
    if ' me ' or 'i ' in sentence:
        return 'self'

    bSimilarity = 5
    bRelational = 'no relational found'
    for relational in relationships:
        for word in nouns:
            similarity = levenshtein(word, relational)
            if similarity < bSimilarity:
                bSimilarity = similarity
                bRelational = relational
    return bRelational


def remove_relational(nouns, relational):
    for noun in nouns:
        if relational in noun or noun in relational:
            nouns.remove(noun)
    return nouns


def get_nouns(sentence):
    possible_objects = list()
    posList = ['NN', 'NNS', 'NNP', 'NNPS']
    tags = tb(to.removePunc(sentence)).tags
    for tag in tags:
        if tag[1] in posList:
            possible_objects.append(tag[0])
    if 'i' in possible_objects:
        possible_objects.remove("i")
    if 'someone' in possible_objects:
        possible_objects.remove("someone")
    return possible_objects
