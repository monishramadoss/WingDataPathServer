import tree_ops as to
import relationals as rel
import bayes as bay
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('-i', "--input", type=str, 
                    default='I need Taco', 
                    help="Input", 
                    required=False
                    )


#import cloud
from urllib import request, parse

'''
data = parse.urlencode({
  "encodingType": "UTF8",
  "document": {
    "type": "PLAIN_TEXT",
    "content": "I need someone to clean my grandmother's kitchen"
  }
}).encode()
req =  request.Request('https://language.googleapis.com/v1/documents:analyzeEntities?key=d6288d15ce82fc8410d5c9245623942b0558000f', data=data) # this will make the method "POST"
'''

def RandomForestRun(inpt):
    nouns = rel.get_nouns(inpt)
    relational = rel.find_relational(inpt, nouns)
    nouns = rel.remove_relational(nouns, relational)
    prediction = bay.bayes_predict(inpt)
    retDict = {"nouns" : nouns, "prediction" : prediction , "relational" : relational}
    return retDict

if(__name__ == '__main__'):
    args, unknown = parser.parse_known_args()
    input = args.input
    output = RandomForestRun(input)
    print(output)
    sys.stdout.flush()