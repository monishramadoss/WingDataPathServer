from sklearn.naive_bayes import BernoulliNB
import numpy as np
from textblob import TextBlob as tb
from textblob_aptagger import PerceptronTagger
from nltk.stem import PorterStemmer
from collections import defaultdict
import csv
import string
from sklearn.externals import joblib
import pickle
import os


def sentence_to_BOW(sentence, word_list):
	lst = list()
	sentence = filter_sentence(sentence)

	for unique_word in word_list:
		if unique_word in sentence:
			lst.append(1)
		else:
			lst.append(0)
	return lst


# filters and stems the given string by each word in said string's Part of Speech
def filter_sentence(sentence):
	stemmer = PorterStemmer()
	newSentence = ''
	stopwords = ['i', 'need', 'someone', 'to', 'my', 'me']
	posList = ['JJ', 'JJS', 'JJR', 'NN', 'NNS', 'NNP', 'NNPS', 'PRP', 'PRP$',
	'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
	tags = tb(removePunc(sentence)).tags
	for tag in tags:
		if tag[1] in posList and tag[0] not in stopwords:
			newSentence += stemmer.stem(tag[0]) + ' '
	return newSentence.strip()


# removes all punctuation from a string
def removePunc(s):
	return ''.join(c for c in s.strip().lower() if c not in string.punctuation)


def getLists(path):
	wordList = list()
	caseList = list()
	reader = csv.reader(open(path))
	sentences, cases, *args = zip(*reader)
	for case, sentence in zip(cases[5:], sentences[5:]):
		if case != '':
			if case not in caseList:
				caseList.append(case)
			sentence = filter_sentence(sentence)
			for word in sentence.split():
				if word not in wordList:
					wordList.append(word)
	return wordList, caseList


def build_Y(path, caseList):
	Y = list()
	reader = csv.reader(open(path))
	sentences, cases, *args = zip(*reader)
	for case, sentence in zip(cases[5:], sentences[5:]):
		Y.append(caseList.index(case))
	return np.array(Y)
	

def build_X(path, wordList):
	X = list()
	reader = csv.reader(open(path))
	sentences, cases, *args = zip(*reader)
	for case, sentence in zip(cases[5:], sentences[5:]):
		if case != '':
			X.append(sentence_to_BOW(sentence, wordList))
	return np.array(X)


def build_bayes_pkl(wordList, caseList):
	clf = BernoulliNB()

	Y = build_Y('t_data.csv', caseList)
	X = build_X('t_data.csv', wordList)

	clf.fit(X, Y)

	joblib.dump(clf, 'bayes.pkl')


def bayes_predict(sentence):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    wordfl = open(dir_path + '/wl.pkl','rb')
    wordList = pickle.load(wordfl)

    casefl = open(dir_path + '/cl.pkl','rb')
    caseList = pickle.load(casefl)

    clf = joblib.load(dir_path + '/bayes.pkl')
    result = clf.predict(np.array([sentence_to_BOW(sentence, wordList)]))
    return caseList[result[0]]


def BuildPickle():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    wordList, caseList = getLists('t_data.csv')

    wordListFile = open(dir_path + 'wl.pkl','wb')
    pickle.dump(wordList,wordListFile)

    caseListFile = open(dir_path + 'cl.pkl','wb')
    pickle.dump(caseList,caseListFile)

    build_bayes_pkl(wordList, caseList)

if __name__ == "__main__":
    BuildPickle()



