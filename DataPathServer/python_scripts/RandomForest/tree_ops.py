from textblob import TextBlob as tb
from nltk.stem import PorterStemmer
import json
import string


# removes all punctuation from a string
def removePunc(s):
	if(s == None):
		return ''
	return ''.join(c for c in s.strip().lower() if c not in string.punctuation)

# filters and stems the given string by each word in said string's Part of Speech
def filter_sentence(sentence):
	stemmer = PorterStemmer()
	newSentence = ''
	stopwords = ['i', 'need', 'someone', 'to', 'my', 'me']
	posList = ['JJ', 'JJS', 'JJR', 'NN', 'NNS', 'NNP', 'NNPS', 'PRP', 'PRP$',
	'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
	tags = tb(removePunc(sentence)).tags
	for tag in tags:
		if tag[1] in posList and tag[0] not in stopwords:
			newSentence += stemmer.stem(tag[0]) + ' '
	return newSentence.strip()

# loads the jungle text file into its original dictionary form
def parseFile(file):
	fileStr = ''
	for line in open(file):
		jg = json.loads(line)
	return jg

# navigates the decision tree generating a yes or no if the sentence
# is determined to be a member of the decision trees class
def testTree(tree, sentence):
	while True:
		if tree == 'YES':
			return 1
		elif tree == 'NO':
			return 0
		if tree['bWord'] in sentence:
			tree = tree['right']
		else:
			tree = tree['left']

# returns the probability of sentence belonging to the forest's class
def testForest(case, forest, sentence):
	prob = 0
	for tree in forest:
		prob += testTree(tree, sentence)
	return prob / len(forest)

# finds the probability of sentence belonging to each case and returns the highest value
def baggingPredict(jungle, sentence):
	bProb, bCase = 0, 'No matching classifiers found'
	sentence = filter_sentence(sentence)
	for case, forest in jungle.items():
		prob = testForest(case, forest, sentence)
		if prob > bProb:
			bProb, bCase = prob, case
	return bCase, bProb
