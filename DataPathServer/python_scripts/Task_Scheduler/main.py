from kafka import KafkaConsumer, KafkaProducer
import multiprocessing as mp
import pymongo
from bson.objectid import ObjectId
import time
url = "mongodb://localhost:27017/"
mongoClient = pymongo.MongoClient(url)
database = mongoClient['wingdb']

task_queue = database['task_queue']
fulfillerdb = database['fulfillers']
taskdb = database['tasks']

consumer = KafkaConsumer('task', group_id='python_task_scheduler')

def sheduler(taskdata):
    return task

def send_postmates(taskdata):
    return task


def producer_task(queue, taskDict):
    
    task_queue_schema = dict ({
        'skill':'',
        'taskIds':[],
        'location': "",
        'working':0,
    });

    while True:
        try:
            task_queueObj = task_queue_schema
            taskid = queue.get()
            taskids = []
            myquery = {'_id': ObjectId(taskid)}
            task = taskdb.find_one(myquery)
            fulfiller = fulfillerdb.find_one({'taskqueId':""})
            if(len(fulfiller) == 0):
                #TODO add postmates Sheduling
                task_queueObj['taskIds'].append(task[0]._id)
                task_queueObj['skill'] = task[0].skill
                x = task_queue.insert_one(task_queueObj)
                               
            else:
                #Task_shedule for fulfiller
                task_queueObj['taskIds'].append(task[0]._id)
                task_queueObj['skill'] = task[0].skill
                x = task_queue.insert_one(task_queueObj)

        except:
            time.sleep(0.0001)

def consumer_task(queue):
    for msg in consumer:
        queue.put(msg.value) #get task_id from kafka

if __name__ == '__main__':
    ctx = mp.get_context('spawn')
    with mp.Manager() as manager:
        q = manager.Queue()
        d = manager.dict()
        producer_process = ctx.Process(target=producer_task, args=(q,))
        consumer_process = ctx.Process(target=consumer_task, args=(q,d,))

        producer_process.start()
        consumer_process.start()

        producer_process.join()
        consumer_process.join()