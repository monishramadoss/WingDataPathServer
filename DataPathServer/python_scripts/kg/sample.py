import pymysql
import DataPath.kg.google_kg
import csv
from collections import defaultdict

GOOGLE_API_KEY = 'AIzaSyCiSxK1R8abeLV5GLHlz55Jv9HicIWTDMo'



def find_object_id(name, cur):

	cur.execute("SELECT * FROM objects WHERE object='%s'" % (name))

	# try in case item isn't in KG
	try:
		return cur.fetchone()[0] # temp is the id of item in KG
	# If not in KG, steal from google
	except: 
		# add item and id to KG, continue with operation
		print("no object '%s' found in knowledge graph. Returning -1" % (name))
		return -1


def find_relation_id(relation_name, cur):
	cur.execute("SELECT * FROM relations WHERE relation='%s'" % (relation_name))

	# try in case item isn't in KG
	try:
		return cur.fetchone()[0] # temp is the id of item in KG
	# If not in KG, steal from google
	except: 
		# add item and id to KG, continue with operation
		print("no relation '%s' found in knowledge graph. Returning -1" % (relation_name))
		return -1

# inserts object and returns its id in the KG
def insert_object(object_name, cur):

	obj_id = find_object_id(object_name, cur)

	# insert object
	if obj_id == -1:
		cur.execute("INSERT IGNORE INTO objects VALUES(null, '%s')" % (object_name))
	else:
		return obj_id
	# retrieve object ID
	return find_object_id(object_name, cur)

# inserts object and returns its id in the KG
def insert_relation(relation_name, cur):
	rel_id = find_relation_id(relation_name, cur)

	# insert object
	if rel_id == -1:
		cur.execute("INSERT IGNORE INTO relations VALUES(null, '%s')" % (relation_name))
	else:
		return rel_id
	# retrieve relation ID
	return find_relation_id(relation_name, cur)


def bulk_insert(file_path, cur):
	reader = csv.reader(open(file_path))
	for row in reader:
		obj1, relation, obj2 = row[0], row[1], row[2]
		if obj1 == '' or relation == '' or obj2 == '':
			print("invalid insertion from csv file: missing data")
		else:
			insert_relationship(obj1, relation, obj2, cur)


def found_at(name, cur):

	name_id = find_object_id(name, cur)

	cur.execute("SELECT object2 FROM object_relation_map WHERE object1='%d' AND relation='%d'" % (name_id, 1))

	obj_id_list = []
	obj_list = []

	for row in cur.fetchall():
		obj_id_list.append(row[0])

	# FIGURE OUT A BETTER WAY TO DO THIS
	for obj_id in obj_id_list:
		cur.execute("SELECT object FROM objects WHERE id='%s'" % (obj_id))
		for obj in cur.fetchall():
			obj_list.append(obj[0])


	return obj_list


def check_duplicate(obj1_id, rel_id, obj2_id, cur):
	
	cur.execute("SELECT * from object_relation_map")
	for row in cur.fetchall():
		if (row[1] == obj1_id) and (row[2] == rel_id) and (int(row[3]) == obj2_id):
			return True
	return False


def insert_relationship(object1, relation, object2, cur):
	
	obj1_id, relation_id, obj2_id = 0,0,0

	# find object1 ID
	obj1_id = find_object_id(object1, cur)

	if(obj1_id == -1):
		obj1_id = insert_object(object1, cur)


	# find relation id
	relation_id = find_relation_id(relation, cur)

	if(relation_id == -1):
		relation_id = insert_relation(relation, cur)
	

	# find object2 id
	obj2_id = find_object_id(object2, cur)

	if(obj2_id == -1):
		obj2_id = insert_object(object2, cur)

	if check_duplicate(obj1_id, relation_id, obj2_id, cur):
		pass
	else:
		cur.execute("INSERT IGNORE INTO object_relation_map VALUES(null, '%d','%d','%d')" % (obj1_id, relation_id, obj2_id))

		cur.execute("SELECT * FROM object_relation_map")



def knowledge_graph_lookup(item_name, cur):
	item_name.lower()

	result = google_kg.lookup(item_name, GOOGLE_API_KEY)

	type_relation = result[0]
	desc_relation = result[1]

	# insert type_relation :
	for item in type_relation[2]:
		insert_relationship(type_relation[0], type_relation[1], item, cur)
		print('\n', type_relation[0], type_relation[1], item)

	insert_relationship(desc_relation[0], desc_relation[1], desc_relation[2], cur)

	print('\n', desc_relation[0], desc_relation[1], desc_relation[2])



def remove_relationship(rel_id, cur):
	cur.execute("DELETE FROM object_relation_map WHERE id='%d'" % (rel_id))



def clean_objects(cur):
	cur.execute("SELECT * FROM objects")

	obj_map = defaultdict(list)

	for row in cur.fetchall():
		obj_map[row[1]].append(row[0])

	for obj in obj_map.keys():
		while(len(obj_map[obj]) > 1):
			cur.execute("DELETE FROM objects where id='%d'" % (obj_map[obj][len(obj_map[obj]) - 1]))
			obj_map[obj].remove(obj_map[obj][len(obj_map[obj]) - 1])



def clean_relations(cur):
	cur.execute("SELECT * FROM relations")

	rel_map = defaultdict(list)

	for row in cur.fetchall():
		rel_map[row[1]].append(row[0])

	for relation in rel_map.keys():
		while(len(rel_map[relation]) > 1):
			cur.execute("DELETE FROM relations where id='%d'" % (rel_map[relation][len(rel_map[relation]) - 1]))
			rel_map[relation].remove(rel_map[relation][len(rel_map[relation]) - 1])


def clean_map(cur):
	cur.execute("SELECT * FROM object_relation_map")
	dup_map = defaultdict(list)

	# map same items to their id in KG
	for row in cur.fetchall():
		dup_map[(row[1], row[2], row[3])].append(row[0])

	# remove items with same relationship id's
	for ids in dup_map.keys():
		while(len(dup_map[ids]) > 1):
			remove_relationship(dup_map[ids][len(dup_map[ids]) - 1], cur)
			dup_map[ids].remove(dup_map[ids][len(dup_map[ids]) - 1])
	

def remove_duplicates(cur):
	clean_objects(cur)
	clean_relations(cur)
	clean_map(cur)



def Main():
	try:
		db = pymysql.connect(host="localhost",    # your host, usually localhost
	                     user="karan",         # your username
	                     passwd="dudeman",  # your password (Nice)
	                     db="pippykb")        # name of the data base

		# Cursor instantiation
		cur = db.cursor()

		print(find_object_id(None, cur))
		

	except pymysql.Error:
		if(db):
			db.rollback()
			print("there was a problem with the sql query")

	# close database connection
	finally:
		if(db):
			db.close()


if __name__ == '__main__':
	Main()