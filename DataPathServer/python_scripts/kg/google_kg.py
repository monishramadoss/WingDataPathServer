import json
import urllib.request
import urllib.parse



def lookup(query, api_key):
    service_url = 'https://kgsearch.googleapis.com/v1/entities:search'

    params = {
        'query': query,
        'limit': 5,
        'indent': True,
        'key': api_key,
    }

    url = service_url + '?' + urllib.parse.urlencode(params)
    response = urllib.request.urlopen(url)
    str_response = response.read().decode('utf-8')
    obj = json.loads(str_response)



    element = obj['itemListElement'][0]

    try:
        type_relation = (element['result']['name'], 'type', element['result']['@type'])
    except KeyError:
        print('Error in parsing Google request; Object has no Type attribute')
        type_relation = None

    try:
        desc_relation = (element['result']['name'], 'description', element['result']['description'])
    except KeyError:
        print('Error in parsing Google request; Object has no description attribute')
        desc_relation = None

    print(type_relation, '\n', desc_relation)

    return type_relation, desc_relation

