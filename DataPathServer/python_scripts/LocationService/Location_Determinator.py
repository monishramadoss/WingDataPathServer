import json
import time
import urllib.request
import urllib.parse
import pprint
FACEBOOK_ACCESS_TOKEN = '651410868391983|PTgj-xdkpbEkvbCqhq9ML4YsQzM'
#GOOGLE_PLACES_API_KEY = 'AIzaSyCiSxK1R8abeLV5GLHlz55Jv9HicIWTDMo'
GOOGLE_PLACES_API_KEY = 'AIzaSyCExeHpnhZ9XGmOlG4pXuQzVISqzjh1j94' # billing Y
#GOOGLE_PLACES_API_KEY = 'AIzaSyAsBoA_dP2XCyFL8SrtVdc5T8cWqSfYddg'
GOOGLE_TEXT_SEARCH_URL = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
GOOGLE_TEXTSEARCH_SAMPLE = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=123+main+street&location=42.3675294,-71.186966&radius=10000&key=YOUR_API_KEY'
#GOOGLE_DISTANCE_API_KEY = 'AIzaSyDD5oRikrKJunGJUW0TLPBA9y12Hyu2Jyk'
GOOGLE_DISTANCE_API_KEY = 'AIzaSyCExeHpnhZ9XGmOlG4pXuQzVISqzjh1j94' # billing Y

#GOOGLE_DISTANCE_API_KEY = 'AIzaSyCiqEHsI-neX7wX6vgvPoJNEVmZutsAxh4'
GOOGLE_DISTANCE_MATRIX_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?'
FACEBOOK_PLACES_URL = 'https://graph.facebook.com/v2.11/search?'
latitude = ' 33.648483'
longitude = '-117.836551'
noun = 'chicken kabob'
noun_with_modifier = 'cheap chicken kabob'



def Google_nearest_address(query_string, lat, lng):
    latlng = str(lat) + ',' + str(lng)

    params = dict(
        query = query_string,
        location = latlng,
        radius = 1000,
        key = GOOGLE_PLACES_API_KEY
    )

    GooglePlacesURL = GOOGLE_TEXT_SEARCH_URL + urllib.parse.urlencode(params)
    response = urllib.request.urlopen(GooglePlacesURL).read().decode()
    response = json.loads(response)

    try:
        if(response['error_message'] != ''):
            return None
    except KeyError:
        try:
            return [response['results'][0]['formatted_address'], response['results'][0]['opening_hours']['open_now']]
        except:
            return None


def GoogleDistance_Matrix(dest, lat, lng):
    latlng = str(lat) + ',' + str(lng)

    params = dict(
        origins = latlng,
        units = 'imperial',
        destinations = dest
    )

    GoogleDistURL = GOOGLE_DISTANCE_MATRIX_URL + urllib.parse.urlencode(params) + '&key=' + GOOGLE_DISTANCE_API_KEY
    json_response = urllib.request.urlopen(GoogleDistURL).read().decode()
    json_response = json.loads(json_response)

    try:
        if json_response['rows'][0]['elements'][0]['status'] == 'NOT_FOUND':
            return None
        else:
                return [json_response['rows'][0]['elements'][0]['distance']['text'], json_response['rows'][0]['elements'][0]['duration']['text']]	  
    except:
        return None

def FaceBook_nearest_address(queryString, lat, lng):
    latlng = str(lat) + ',' + str(lng)
    params = dict(
        type = 'place',
        q = queryString,
        center = latlng,
        distance = '10000',
        fields = 'location,name',
        access_token = FACEBOOK_ACCESS_TOKEN
    )

    FacebookQueryUrl = FACEBOOK_PLACES_URL + urllib.parse.urlencode(params)
    print(FacebookQueryUrl)
    response = urllib.request.urlopen(FacebookQueryUrl).read().decode()
    response = json.loads(response)
    full_address = ''
    
    try:
        full_address += '' + response['data'][0]['location']['street'] + ','
        full_address += response['data'][0]['location']['state'] + ','
        full_address += response['data'][0]['location']['zip']
        return full_address
    except IndexError:
        return None


def Location_Service(Destlst,lat,lng):
    tim = time.time()
    response = {"places" : Destlst, "AddressSpace":list(), "googleDistance" : list()}

    for dest in Destlst:
        googlePlace = Google_nearest_address(dest,lat,lng)
        if(googlePlace == None):
            response['AddressSpace'].append( ["Not able to acceess GOOLGE API",1000000] )#FaceBook_nearest_address(dest,lat,lng))
        else:
            response['AddressSpace'].append(googlePlace)

    for i in range(0, len(Destlst)):
        if(response['places'][i] != None):
            response['googleDistance'].append(GoogleDistance_Matrix(response['AddressSpace'][i],lat,lng))
        else:
            response['googleDistance'].append(None)
    pp = pprint.PrettyPrinter(depth=6)
    try:
        response['places'] = [x for x,_ in sorted(zip(response['places'],response['googleDistance']), key = lambda p: p[1])]
        response['AddressSpace'] = [x for x,_ in sorted(zip(response['AddressSpace'],response['googleDistance']), key = lambda p: p[1])]
        response['googleDistance'] = sorted(response['googleDistance'], key=lambda x:x[0]) 
    except:
        pass

    return response
