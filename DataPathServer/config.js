﻿var config = {
    mongodb: ["mongodb://localhost:27017/"],
    kafka_server: {
        kafka_host: "localhost:9092",
        kafka_start: "C:\\kafka_2.11-2.1.0\\bin\\windows\\kafka-server-start.bat",
        kafka_start_config: "C:\\kafka_2.11-2.1.0\\config\\config\\server.properties",
        kafka_zookeeper: "C:\\kafka_2.11-2.1.0\\bin\\windows\\zookeeper-server-start.bat",
        kafka_zookeeper_config: "C:\\kafka_2.11-2.1.0\\config\\zookeeper.properties",
    },
}

module.exports = config;