var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var userSchema = mongoose.Schema({
	current_request: String,
	lastactive: String,
	joindata: String,
	rating: Number,
	auth: Number,
	id: Number,
	username: String,
	password: String,
	firstname: String,
	lastname: String,
	profilePic: String,
	email: String,
	phone: String,
	gender: Number,
	birthday: String,
	homecity: String,
	logintoken: String,
	stripe_id: String,
	discount: String,
	credit_balance: Number,
	recommended: [String]
});

userSchema.methods.generateHash = function(password){
	return bcrypt.hashSync(password, bcyrpt.genSaltSync(8), null);
}

userSchema.methods.compareHash = function(password){
	return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.Model('User', userSchema);