﻿var express = require('express');
var router = express.Router();
var fs = require('fs');
var emoji = require('node-emoji');


router.get('/:sent', function (req, res) {

    var user_request = req.params.sent    
    var spawn = require('child_process').spawn;
    user_request = emoji.unemojify(user_request).replace(':', '');   
    var pythonScript = __dirname + '/../python_scripts/RandomForest/runner.py';
    var pythonProcess = spawn('python', [pythonScript, "-i " + user_request])
       
    pythonProcess.stdout.on('data', function (data) {
        var bayes_output = data.toString('utf-8');
        var json = JSON.stringify(eval("(" + bayes_output + ")"));
        var logString = "{Requrest: " + String(user_request) + ", Response: " + data.toString('utf-8') + "}\n";
        fs.appendFile('./logs/request_response.log', new Buffer(logString), function (err) { });
        res.send(json);
    });

    pythonProcess.stdout.on('end', function (data) {
        var utput = data;
    });

    pythonProcess.stderr.on('data', function (data) {
        var logString = "{Requrest: " + String(user_request) + ", Response: ERROR:" + data.toString('utf-8') + "}\n";
        console.log(data.toString('utf-8'));
        fs.appendFile('./logs/error.log', new Buffer(logString), function (err) { });
        var upt = data;
    })
        
});

router.get('/:lat/:lng', function (req, res) {
    var lat = req.params.lat;
    var lng = req.params.lng;
    res.send("I know where you are");
});


module.exports = router;