﻿var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var bcrypt = require('bcrypt');
var User = require('../models/user.js');

var clientSchema = {
    current_request: "",
    lastactive: "",
    joindata: "",
    rating: 5,
    auth: 0,

    username: "",
    firstname: "",
    password: "",
    profilePic: "",
    email: "",
    phone: "",
    gender: 0,
    birthday: "",
    homecity: "",
    logintoken: "",
    stripe_id: "",     
    discount: "",
    credit_balance: 0,    
    recommended: [],
};

/*
router.post('/login', function(req, res){
    var clientObj = {};
    for(key in req.body){
        clientObj[key] = req.body[key];
    }
    res.setHeader('Content-Type', 'application/json');
    var query = {username: req.body.username};
    
});
*/

router.post('/login', function(req, res){
    var username = req.body.username;
    var password = req.body.password;
    User.findOne({username: username}, function(err, user){
        if(err) res.status(500).send(err);
        else if(!user) res.json({result: 'Username not found'});
        else{
            if(user.compareHash(password)){
                res.json({result: 'Validated', id: user.id});
            }
            else res.json({result: 'Wrong password'});
        }
    });
});

router.get('/user/:id', function(req, res){
    var userId = req.params.id;
    User.findOne({id: userId}, function(err, user){
        if(err) res.status(500).send(err);
        else if(!user) res.json({result: 'User not found'});
        else res.json(user);
    });
})

router.get('/login/:username/:userid/', function (req, res) {
    //TODO
    var clientObj = {};
    for (key in req.body) {
        clientObj[key] = req.body[key];
    }

    res.setHeader('Content-Type', 'application/json');
    var query = { username: req.params.username, _id: mongodb.ObjectId(req.params.userid) };

    req.app.locals.db.collection('users').find(query).toArray(function (err, result) {
        if (err) console.error(err);
        res.json({ result: result });
    });

});

router.post('/add_user', function (req, res) {
    //needs to be checked if user exists already before insert
    var clientObj = clientSchema;
    for (key in req.body) {
        clientObj[key] = req.body[key];
    }

    req.app.locals.db.collection('users').insertOne(clientObj, function (err, results) {
        if (err) console.error(err);
        res.send(clientObj._id);
    });

});


router.put('/update_user/:username/:userid', function (req, res) {
    var clientObj = {};
    for (key in req.body) {
        clientObj[key] = req.body[key];
    }

    var query = { username: req.params.username, _id: mongodb.ObjectId(req.params.userid) };

    req.app.locals.db.collection('users').updateOne(query, { $set: clientObj }, { upsert: false }, function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: result.matchedCount,
        };
        res.json({ result: output });
    });


});


router.get('/find_user/:username/:userid', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var query = { username: req.params.username, _id: mongodb.ObjectId(req.params.userid) };

    req.app.locals.db.collection('users').find(query).toArray(function (err, result) {
        if (err) console.error(err);
        res.json({ result: result });
    });
});


router.get('/', function (req, res) {

});


module.exports = router;