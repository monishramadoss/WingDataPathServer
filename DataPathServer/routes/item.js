var express = require('express');
var router = express.Router();

var itemSchema = {
    name: "",
    description: "",
    image: "",
    price: 0.0,
    foundat: []
};


router.post('/add_item', function (req, res) {
    //needs to be checked if user exists already before insert
    var itemObj = itemSchema;

    for (key in req.body) {
        itemObj[key] = req.body[key];
    }

    req.app.locals.db.collection('items').insertOne(itemObj, function (err, results) {
        if (err) console.error(err);
        res.send("1");
    });
});


router.put('/update_item/:name', function (req, res) {
    var query = { name: req.params.name };
    var itemObj = {};
    for (key in req.body) {
        itemObj[key] = req.body[key];
    }

    req.app.locals.db.collection('items').updateOne(query, { $set: itemObj }, { upsert: false }, function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: result.matchedCount,
        };
        res.json({ result: output });
    });
});


router.get('/find_item/:name', function (req, res) {
    var query = { name: req.params.name};
    req.app.locals.db.collection('items').find(query).toArray(function (err, results) {
        if (err) console.error(err);
        res.json({ result: results });
    });
});


router.get('/', function (req, res) {

});


module.exports = router;