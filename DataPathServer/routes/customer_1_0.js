﻿var express = require('express');
var router = express.Router();
var kafka = require('node-rdkafka');
var config = require('../config');
var mongodb = require('mongodb');

var taskSchema = {
    userid: "",
    fulfillerid: "",
    status: "", //complete, in-route, beginning, in-queue
    creation_time: "",

    from: "",
    to: "",
    discount: "",
    skill: "",
    quoted_price: "",
    estimated_completion_time: "",
    items: [],

};   


router.post('/add_task/:username/:userid', function (req, res) {
    var producer = new kafka.Producer({
        'client.id': 'python_task_scheduler',
        'metadata.broker.list': config.kafka_server.kafka_host,
        'dr_cb': true,
    });
    producer.connect();

    var d = new Date();

    var taskObj = taskSchema;
    for (key in req.body) {
        taskObj[key] = req.body[key];
    }
    
    taskObj.status = "in-queue";
    taskObj.userid = req.params.userid;
    taskObj.creation_time = d.toUTCString();

    var query = { username: req.params.username, _id: mongodb.ObjectId(req.params.userid) };

    req.app.locals.db.collection('tasks').insertOne(taskObj, function (err, results) {
        if (err) console.error(err);
        req.app.locals.db.collection('users').updateOne(query, { $set: { current_request: taskObj._id } }, { upsert: false }, function (err, results) {
            if (err) console.error(err);
            producer.on('ready', function () {
                producer.produce('task', -1, Buffer.from(taskObj._id), 0);
            });
            producer.on('error', function (err) {
                console.error(err);
            });
            res.send(taskObj._id);
        });
    });
       
   

});


router.put('/update_task/:username/:userid/:current_request', function (req, res) {
    var taskObj = {};
    for (key in req.body) {
        taskObj[key] = req.body[key];
    }

    var query = { userid: req.params.userid, _id: mongodb.ObjectId(req.params.current_request) };
   
    req.app.locals.db.collection('tasks').updateOne(query, { $set: taskObj }, { upsert: false }, function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: result.matchedCount,
        };
        res.json({ result: output });
    });
    
});


router.get('/find_task/:username/:userid/:current_request', function (req, res) {

    var query = { userid: req.params.userid, _id: mongodb.ObjectId(req.params.current_request) };
    
    req.app.locals.db.collection('tasks').find(query).toArray(function (err, result) {
        if (err) throw err;
        res.json({ result: result });
    });
   
});


router.get('/', function (req, res) {
  
});


module.exports = router;