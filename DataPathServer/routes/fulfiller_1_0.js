﻿var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');

var fulfillerSchema = {
    username: "",
    userid: "",
    active: 0,
    rating: 5,
    taskqueId: "",
    skills: [],
};

var task_queueSchema = {
    working: 0,
    skill: "",
    location: "",
    taskIds: []
};


router.get('/give_task/:username/:userid', function (req, res) {
    var query = { username: req.params.username, userid: req.params.userid };
    var s = req.body['skills'];
    var skills = [];

    for (skill in s) {
        skills.push({ 'skill': skill });
    }

    req.app.locals.db.collection('fulfillers').find(query).toArray(function (err, result) {
        if (err) console.error(err);
        if (result.length > 0) {
            req.app.locals.db.collection('task_queue').find({ $and: [{ $or: skills }, { working: 0 }] }).toArray(function (err, results) {
                if (err) console.error(err);
                for (var i = 0; i < results.length; ++i) {
                    if (results[i].working == 0) {
                        res.json(results);
                        break;
                    }
                }
            });
        }
        else {
            res.send('invalid');
        }

    });
});


router.post('/accept_task/:username/:userid/:taskqueid', function (req, res) {
    var query = { username: req.params.username, userid: req.params.userid, active: 1 };
    var task_queue_query = { _id: mongodb.ObjectId(req.params.taskqueid), working: 0 };
    req.app.locals.db.collection('fulfillers').updateOne(query, { $set: { taskqueId: req.params.taskqueid } }, { upsert: false }, function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: results.matchedCount,
        };
        res.json({ result: output });
    });

    req.app.locals.db.collection('task_queue').updateOne(task_queue_query, { $set: { working: 1 } }, { upsert: false }, function (err, result) {
        if (err) console.error(err);    
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: results.matchedCount,
        };

    });

    req.app.locals.db.collection('task_queue').find(task_queue_query).toArray(function (err, results) {
        if (err) console.error(err);
        for (id in results[0]['taskIds']) {
            req.app.locals.db.collection('tasks').updateOne({ _id: mongodb.ObjectId(id) }, { $set: { fulfillerid: req.params.userid } }, { upsert: false }, function (err, result) {
                if (err) console.error(err);
                var output = {
                    upserted: result.upsertedCount,
                    modified: result.modifiedCount,
                    matched: results.matchedCount,
                };

            });
        }
    });

});


router.post('/complete_task/:username/:userid/:taskqueid/:taskid', function (req, res) {
    var query = { username: req.params.username, userid: req.params.userid, active: 1 };
    var task_queue_query = { _id: mongodb.ObjectId(req.params.taskqueid) };    
    
    req.app.locals.db.collection('tasks').updateOne({ _id: mongodb.ObjectId(req.params.taskid) }, { $set: { status: "complete" } }, { upsert: false }, function (err, ress) {
        if (err) console.error(err);
        req.app.locals.db.collection('task_queue').updateOne(task_queue_query, { $pull: { 'taskIds': req.params.taskid } }, { upsert: false }, function (err, result) {
            if (err) console.error(err);
            if (result.taskIds.length == 0) {
                req.app.locals.db.collection('fulfillers').updateOne(query, { $set: { taskqueId: "" } }, { upsert: false }, function (err, result) {
                    if (err) console.error(err);
                });                
            }
        });
    });

    /*
    req.app.locals.db.collection('task_queue').deleteMany({ 'taskIds': { $size: 0 }}, function (err, result) {
        if (err) console.error(err);
    });  
    */

});


router.post('/add_fulfiller/:username/:userid', function (req, res) {
    
    fulfillerObj = fulfillerSchema;
    fulfillerObj['username'] = req.params.username;
    fulfillerObj['userid'] = req.params.userid;
    fulfillerObj['active'] = 0;
    fulfillerObj['skills'] = req.body['skills'];

    var query = { username: req.params.username, _id: mongdb.ObjectId(req.params.userid) };

    req.app.locals.db.collection('users').find(query).toArray(function (err, result) {
        if (err) console.error(err);
        if (result.length == 0) { res.send('user not found'); }
        if (result.length != 0) {
            req.app.locals.db.collection('fulfillers').insertOne(fulfillerObj, function (err, result) {
                if (err) console.error(err);
                res.send(fulfillerObj._id);
            });
        }

    });
});


router.post('/go_online/:username/:userid', function (req, res) {
    var query = { username: req.params.username, userid: req.params.userid };
    req.app.locals.db.collection('fulfillers').updateOne(query, { $set: { active: 1 } }, { upsert: false }, function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: results.matchedCount,
        };
        res.json({ result: output });
    });
});


router.post('/go_offline/:username/:userid', function (req, res) {
    var query = { username: req.params.username, userid: req.params.userid };
    req.app.locals.db.collection('fulfillers').updateOne(query, { $set: { active: 0 } },  { upsert: false },function (err, result) {
        if (err) console.error(err);
        var output = {
            upserted: result.upsertedCount,
            modified: result.modifiedCount,
            matched: results.matchedCount,
        };
        res.json({ result: output });
    });
  
});


router.get('/', function (req, res) {

});

module.exports = router;