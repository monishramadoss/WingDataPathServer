﻿var express = require('express');
var axios = require('axios');
var router = express.Router();

//'https://maps.googleapis.com/maps/api/place/textsearch/json?query=123+main+street&location=42.3675294,-71.186966&radius=10000&key=AIzaSyAsBoA_dP2XCyFL8SrtVdc5T8cWqSfYddg'
//var GOOGLE_PLACES_API_KEY = 'AIzaSyCiSxK1R8abeLV5GLHlz55Jv9HicIWTDMo'
var GOOGLE_PLACES_API_KEY = 'AIzaSyCExeHpnhZ9XGmOlG4pXuQzVISqzjh1j94'  // billing Y
//var GOOGLE_PLACES_API_KEY = 'AIzaSyAsBoA_dP2XCyFL8SrtVdc5T8cWqSfYddg'
//var GOOGLE_DISTANCE_API_KEY = 'AIzaSyDD5oRikrKJunGJUW0TLPBA9y12Hyu2Jyk'
var GOOGLE_DISTANCE_API_KEY = 'AIzaSyCExeHpnhZ9XGmOlG4pXuQzVISqzjh1j94' // billing Y
//var GOOGLE_DISTANCE_API_KEY = 'AIzaSyCiqEHsI-neX7wX6vgvPoJNEVmZutsAxh4'
var FACEBOOK_ACCESS_TOKEN = '651410868391983|PTgj-xdkpbEkvbCqhq9ML4YsQzM'


async function locationService(query_string, lat, lng, req, res) {
    var url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' + query_string + '&location=' + String(lat) + "," + String(lng) + '&radius=1000&key=' + GOOGLE_PLACES_API_KEY;
    
    const distance_matrix = async (formated_addr, destination) => {
        try {
            var url = 'https://maps.googleapis.com/maps/api/distancematrix/json?destinations=' + formated_addr + '&origins=' + String(lat) + ',' + String(lng) + '&units=imperial&key=' + GOOGLE_DISTANCE_API_KEY;
            const response = await axios.get(url);
            var res = { "destination": destination, "duration": response.data };
            return res;
        }
        catch (error) {
            console.log(error);
        }
    };

    const facebook_text_search = async () => {
        try {
            var url = "https://graph.facebook.com/v2.11/search?type=place&fields=location,name&q=" + query_string + '&center=' + String(lat) + "," + String(lng) + '&distance=1000&access_token=' + FACEBOOK_ACCESS_TOKEN;
            const response = await axios.get(url);
            return response.data;
        }
        catch (error) {
            console.log(error);
        }
    };

    const text_search = async () => {
        try {
            const response = await axios.get(url);
            var results = response.data.results;
            var response_list = { "LocationService": [] };
            var promises = []

            if (results.length == 0) {
                //response = await facebook_text_search();
            }

            for (var i = 0; i < results.length; ++i) {
                if (results[i].hasOwnProperty('formatted_address')) {
                    var addr = results[i]['formatted_address'];
                    var formated_addr = addr.split(' ').slice(0, -3).join('+').slice(0, -1);
                    promises.push(distance_matrix(formated_addr, results[i]));
                }
            }

            var responses = await axios.all(promises);
            responses.forEach(function (response) {
                response_list['LocationService'].push(response)
            });

            return response_list;
        }
        catch (error) {
            console.log(error);
        }

    };

    var response_list = await text_search();
    res.json(response_list);
  
}




router.use('/:query/:lat/:lng', function (req, res) {
    var user_request = req.params.query
    var lat = req.params.lat;
    var lng = req.params.lng;
    locationService(user_request, lat, lng, req, res);
});

router.use('/', function (req, res) {
    var query = 'taco'; //"taco";
    var lat = 33.648201;
    var lng = -117.840538;
    locationService(query, lat, lng, req, res)     
});


module.exports = router;